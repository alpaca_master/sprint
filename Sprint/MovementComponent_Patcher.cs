﻿using Harmony;
using UnityEngine;
using System.Collections;
using System;

namespace Sprint
{
    [HarmonyPatch(typeof(MovementComponent))]
    [HarmonyPatch("UpdateMovement")]
    internal class MovementComponent_UpdateMovement_Patch
    {
        [HarmonyPostfix]
        public static void Postfix(MovementComponent __instance)
        {
            var path = @"./QMods/Sprint/config.txt";
            System.IO.StreamReader file = new System.IO.StreamReader(path);

            string line = file.ReadLine(); // skip toggleMode line

            line = file.ReadLine();
            string[] options = line.Split('=');
            float energyCost = (float)Convert.ToDouble(options[1]);

            line = file.ReadLine();
            options = line.Split('=');
            float speed = (float)Convert.ToDouble(options[1]);

            bool toggleMode = Configuration.getInstance().getToggleMode();
            if (toggleMode)
            {
                if (__instance.wgo.is_player)
                {
                    if (MainGame.me.player.GetParam("isSprinting") == 1)
                    {
                        __instance.SetSpeed(speed);
                        MainGame.me.player.energy -= energyCost;
                    }
                    else
                    {
                        if (__instance.wgo.is_player)
                        {
                            __instance.SetSpeed(3.0f);
                        }
                    }
                }
            }

            else
            {

                if (Input.GetKey(KeyCode.LeftShift))
                {

                    if (__instance.wgo.is_player)
                    {
                        __instance.SetSpeed(speed);
                        MainGame.me.player.energy -= energyCost;
                    }
                }
                if (!Input.GetKey(KeyCode.LeftShift))
                {
                    if (__instance.wgo.is_player)
                    {
                        __instance.SetSpeed(3.0f);
                    }
                }
            }


        }


    }
}